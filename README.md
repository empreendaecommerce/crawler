# Crawler

## Referência

### Instalação


Os objetos foram criados dentro do namespace Truo\Crawler caso sua aplicação não tenha isso é só copiar e alterar o namespace.
Caso queira usar o pacote a partir do repositório é só adicionar no composer.json do laravel:

```json
"repositories": [
    {
        "type": "vcs",
        "url":  "https://git.pentagrama.net.br/truo/crawler.git"
    }
],
"require": {
    "truo/crawler": "dev-master"
},
```

#### Testes

Para rodar o arquivo de testes, test.php, você deve rodar o composer para criar a estrutura de autoload:

`php composer.phar dump-autoload`

E abrir normalmente o arquivo `test.php` no browser.

### Instância

Cada objeto referencia uma página de produto da aliexpress, dessa forma, o objeto pode ser passado para frente para reutilizar.

```php
use Truo\Crawler\Aliexpress;

/**
 * @param $aliexpress_product_url URL do produto da AliExpress
 */
$crawler = new Aliexpress($aliexpress_product_url);
```

### $crawler->getBasicInfo()

Retorno:
```php
array(
  'title' => 'SAMSUNG USB 3.0 BAR 128 GB 64 GB 32 GB USB Flash Drive de Disco Pen Drive Pendrive Memory Stick U Disco 128G 64G 32G 100% Original',
  'objectId' => '32819904152'
);
```

### $crawler->getImages()

Retorno:
```php
array(
    array(
        'thumbnail' => 'https://ae01.alicdn.com/kf/HTB1JkOCSXXXXXctXVXXq6xXFXXXz/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'image' => 'https://ae01.alicdn.com/kf/HTB1JkOCSXXXXXctXVXXq6xXFXXXz/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg'
    ),
    array(
        'thumbnail' => 'https://ae01.alicdn.com/kf/HTB1e51cSFXXXXcWXXXXq6xXFXXX8/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'image' => 'https://ae01.alicdn.com/kf/HTB1e51cSFXXXXcWXXXXq6xXFXXX8/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg'
    ),
    array(
        'thumbnail' => 'https://ae01.alicdn.com/kf/HTB1X_dQSFXXXXX6XVXXq6xXFXXXr/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'image' => 'https://ae01.alicdn.com/kf/HTB1X_dQSFXXXXX6XVXXq6xXFXXXr/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg'
    ),
    array(
        'thumbnail' => 'https://ae01.alicdn.com/kf/HTB18gJrSFXXXXcMapXXq6xXFXXXx/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'image' => 'https://ae01.alicdn.com/kf/HTB18gJrSFXXXXcMapXXq6xXFXXXx/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg'
    ),
    array(
        'thumbnail' => 'https://ae01.alicdn.com/kf/HTB1iU8RSFXXXXXjXVXXq6xXFXXX1/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'image' => 'https://ae01.alicdn.com/kf/HTB1iU8RSFXXXXXjXVXXq6xXFXXX1/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg'
    ),
    array(
        'thumbnail' => 'https://ae01.alicdn.com/kf/HTB1NPpRSFXXXXXOXVXXq6xXFXXXY/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'image' => 'https://ae01.alicdn.com/kf/HTB1NPpRSFXXXXXOXVXXq6xXFXXXY/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg'
    )
)
```

### $crawler->getProductSKUs($objectId)

```php
array(
    array(
        'product_id' => '32819904152',
        'sku_id' => '5116,366,201336100',
        'sku_title' => '26:5116;14:366#Blue Gift Lanyard;200007763:201336100',
        'sku_stock' => 18,
        'sku_price' => '47.72',
        'sku_image' => 'https://ae01.alicdn.com/kf/HTB1BOOtb49YBuNjy0Ffq6xIsVXaQ/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg',
        'sku_thumbnail' => 'https://ae01.alicdn.com/kf/HTB1BOOtb49YBuNjy0Ffq6xIsVXaQ/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'simplo7_sku' => '',
        'simplo7_sku_fornecedor' => '',
        'simplo7_quantidade' => '',
        'simplo7_valor_compra' => '',
    ),
    array(
        'product_id' => '32819904152',
        'sku_id' => '5116,366,201336103',
        'sku_title' => '26:5116;14:366#Blue Gift Lanyard;200007763:201336103',
        'sku_stock' => 0,
        'sku_price' => '59.75',
        'sku_image' => 'https://ae01.alicdn.com/kf/HTB1BOOtb49YBuNjy0Ffq6xIsVXaQ/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg',
        'sku_thumbnail' => 'https://ae01.alicdn.com/kf/HTB1BOOtb49YBuNjy0Ffq6xIsVXaQ/SAMSUNG-USB-3-0-BAR-128-GB-64-GB-32-GB-USB-Flash-Drive-de-Disco.jpg_50x50.jpg',
        'simplo7_sku' => '',
        'simplo7_sku_fornecedor' => '',
        'simplo7_quantidade' => '',
        'simplo7_valor_compra' => '',
    ),
)
```
