<?php

class AliExpressController extends \BaseController {

	public static function CRON_fetchProductDetails() {

		// Fetch first non-published products

		$products = Product::where('deleted_at')
							->orderBy('simplo7_id','ASC')
							->get();

		if ( $products && $products->count() > 0 ) :

			foreach ( $products as $item ) :

				// SKUs

				$skus = self::getProductSKUs( $item->id, $item->product_url, false );
				if ( $skus ) :
					$total_skus = count($skus);

					if ( $total_skus > 0 ) {

						for ( $s=0;$s<$total_skus;++$s ) :

							$the_sku = ProductVariation::where('product_id','=',$item->id)
														->where('sku_id','=',$skus[$s]['sku_id'])
														->update(
															array(
																'sku_stock' => $skus[$s]['sku_stock'],
																'sku_price' => $skus[$s]['sku_price']
															)
														);

							// AppHelper::logThis( 'Alterou o produto: '.$item->id.'. Stock: '.$skus[$s]['sku_stock'].' Price: '.$skus[$s]['sku_price'] );

						endfor;

					}

				endif;

			endforeach;

		endif;

	}


	public static function getProductBasicInfos( $product_url )
	{
		if ( empty($product_url) || !AppHelper::is_url( $product_url ) )
			return false;

		$title = 'Título não encontrado';

		try {

			$page_content = file_get_contents( $product_url );
			$page_content = trim( preg_replace('/\s+/', ' ', $page_content) );

			// Check if is a login page

			// Possible pageTypes:
			// main_detail_bra (when URL has item)
			// store-detail (when URL has store)

			preg_match("/(pageType\:\ \'wslogin\')/", $page_content, $is_login);
			if ( isset($is_login[0]) && !empty($is_login[0]) ) :
				return false;

			else :
				
				// 
				// Product title
				// 

				preg_match("/\<h1\ class\=\"product\-name\"\ itemprop\=\"name\"\>(.*)\<\/h1\>/i", $page_content, $title);
				if ( isset($title[1]) && !empty($title[1]) ) {
					$title = $title[1];
					$title = utf8_encode($title);
					$title = html_entity_decode($title);
				}

				if ( empty($title) || $title == NULL )
					$title = 'Título não encontrado';

				$product_infos = array(
					'title' => $title
				);

				AppHelper::logThis( $product_infos );

				return $product_infos;

			endif;

		} catch ( Exception $e ) {

			AppHelper::logThis( $e );

			return false;

		}
	}



	public static function getProductImages( $product_id, $product_url )
	{

		if ( empty($product_url) || !AppHelper::is_url( $product_url ) )
			return false;

		try {

			$page_content = file_get_contents( $product_url );
			$page_content = trim( preg_replace('/\s+/', ' ', $page_content) );

			// Get the canonical URL

			/*
			// Não está adiantando pois ao chegar na página da /store/ já considera uma página de login
			preg_match("/(\<link\ href\=\"https\:\/\/)(.+)(rel\=\"canonical\")/", $page_content, $canonical_url);
			if ( isset($canonical_url[0]) && !empty($canonical_url[0]) ) {
				$canonical_url = $canonical_url[0];

				$canonical_url = str_replace('<link href="','',$canonical_url);
				$canonical_url = str_replace('rel="canonical"','',$canonical_url);
				$canonical_url = str_replace('"','',$canonical_url);
				$canonical_url = trim($canonical_url);

				echo 'Dentro do IF: '.$canonical_url;
				dd();

				if ( AppHelper::is_url( $canonical_url ) ) {
					$page_content = file_get_contents( $canonical_url );
					$page_content = trim( preg_replace('/\s+/', ' ', $page_content) );
				}
			}
			*/

			// Check if is a login page

			preg_match("/(pageType\:\ \'wslogin\')/", $page_content, $is_login);
			if ( isset($is_login[0]) && !empty($is_login[0]) ) :
				return false;

			else :

				$thumbnail_sku_id = '';
				$thumbnail_id = '';
				$thumbnail_title = '';
				$thumbnail_image = '';

				// Reference regex tester: https://regex101.com/

				preg_match("/(window\.runParams\.imageBigViewURL\=)(.*)(window\.runParams\.mainBigPic)/i", $page_content, $cover);

				if ( isset($cover[0]) && !empty($cover[0]) ) :

					$cover = $cover[0];

					$cover = preg_replace("/(window\.runParams\.imageBigViewURL\=\[\ )/i","", $cover);
					$cover = preg_replace("/(\]\;\ window\.runParams\.mainBigPic)/i","", $cover);
					$cover = preg_replace("/\"/i","", $cover);
					$cover = preg_replace("/\ /i","", $cover);

					$cover = explode(',', $cover);
					$total_images = count($cover);

					for ( $i=0;$i<$total_images;++$i ) :

						$thumbnail_image = trim( $cover[$i] );

						if ( !empty($thumbnail_image) ) {

							$thumbnail_generated_name = AppHelper::downloadProductImage( $thumbnail_image );

							$full_image_url = Config::get('app.api_url').'products/';

							$images_array[] = array(
								'product_id' => $product_id,
								'image_url' => $full_image_url.$thumbnail_generated_name
							);

						}

					endfor;

					if ( empty($images_array) )
						return false;

					return $images_array;

				endif;

			endif;

		} catch ( Exception $e ) {

			return false;

		}

	}



	public static function getProductSKUs( $product_id, $product_url, $should_download_images = true ) {

		// 
		// Get product variations
		// 

		if ( empty($product_url) || !AppHelper::is_url( $product_url ) )
			return false;

		try {

			$page_content = file_get_contents( $product_url );
			$page_content = trim( preg_replace('/\s+/', ' ', $page_content) );

			// Check if is a login page

			preg_match("/(pageType\:\ \'wslogin\')/", $page_content, $is_login);
			if ( isset($is_login[0]) && !empty($is_login[0]) ) :
				return false;

			else :

				// AppHelper::logThis( 'vai buscar os SKUs, menino!!!');

				// linha 593
				// var skuProducts=[{"skuAttr":"73:175#HKj886black gray","skuPropIds":"175","skuVal":{"actSkuCalPrice":"4.45","actSkuMultiCurrencyCalPrice":"15.11","actSkuMultiCurrencyDisplayPrice":"15,11","availQuantity":1770,"inventory":1998,"isActivity":true,"skuCalPrice":"8.09","skuMultiCurrencyCalPrice":"27.46","skuMultiCurrencyDisplayPrice":"27,46"}},{"skuAttr":"73:10#HKj886silver blue","skuPropIds":"10","skuVal":{"actSkuCalPrice":"4.45","actSkuMultiCurrencyCalPrice":"15.11","actSkuMultiCurrencyDisplayPrice":"15,11","availQuantity":1985,"inventory":2000,"isActivity":true,"skuCalPrice":"8.09","skuMultiCurrencyCalPrice":"27.46","skuMultiCurrencyDisplayPrice":"27,46"}},{"skuAttr":"73:173#HKj886gold pink","skuPropIds":"173","skuVal":{"actSkuCalPrice":"4.45","actSkuMultiCurrencyCalPrice":"15.11","actSkuMultiCurrencyDisplayPrice":"15,11","availQuantity":1955,"inventory":1999,"isActivity":true,"skuCalPrice":"8.09","skuMultiCurrencyCalPrice":"27.46","skuMultiCurrencyDisplayPrice":"27,46"}},{"skuAttr":"73:366#HKj886gold red","skuPropIds":"366","skuVal":{"actSkuCalPrice":"4.45","actSkuMultiCurrencyCalPrice":"15.11","actSkuMultiCurrencyDisplayPrice":"15,11","availQuantity":1952,"inventory":1999,"isActivity":true,"skuCalPrice":"8.09","skuMultiCurrencyCalPrice":"27.46","skuMultiCurrencyDisplayPrice":"27,46"}},{"skuAttr":"73:496#HKj886coffee gold","skuPropIds":"496","skuVal":{"actSkuCalPrice":"4.45","actSkuMultiCurrencyCalPrice":"15.11","actSkuMultiCurrencyDisplayPrice":"15,11","availQuantity":1953,"inventory":1999,"isActivity":true,"skuCalPrice":"8.09","skuMultiCurrencyCalPrice":"27.46","skuMultiCurrencyDisplayPrice":"27,46"}},{"skuAttr":"73:29#HKj886silver silver","skuPropIds":"29","skuVal":{"actSkuCalPrice":"4.45","actSkuMultiCurrencyCalPrice":"15.11","actSkuMultiCurrencyDisplayPrice":"15,11","availQuantity":1989,"inventory":2000,"isActivity":true,"skuCalPrice":"8.09","skuMultiCurrencyCalPrice":"27.46","skuMultiCurrencyDisplayPrice":"27,46"}}];

				// linha 783
				// var skuAttrIds=[[175,10,173,366,496,29]];

				// 
				// SKUs
				// https://jsonformatter.org/
				// 

				// preg_match("/(var\ skuProducts\=\[\{)(.*)(\"\}\}\]\;)/i", $page_content, $skus);
				preg_match("/(var\ skuProducts\=\[\{)(.*)(}\}\]\;)/i", $page_content, $skus);
				if ( !isset($skus[2]) || empty($skus[2]) )
					return false;

				$skus = $skus[2];
				$skus_json = '[{'.$skus.'}}]';

				// AppHelper::logThis('SKUs em JSON: '.$skus_json);

				$skus_json = json_decode( $skus_json );

				// id=\"sku-1-175\" 
				// SKU <li> element in Ali Express: '#sku-1-'.$sku_id

				$total_skus = count( $skus_json );

				for ( $is=0;$is<$total_skus;++$is ) {

					$sku_id = $skus_json[$is]->skuPropIds;
					$sku_title = $skus_json[$is]->skuAttr;
					$sku_stock = $skus_json[$is]->skuVal->availQuantity;
					$sku_current_price_brl = $skus_json[$is]->skuVal->skuMultiCurrencyCalPrice;
					// $sku_current_price_brl = 0.0;
					// $sku_current_price = $skus_json[$i]->skuVal->SkuCalPrice; // Comentado

					$item_skus[] = array(
						'product_id' => $product_id,
						'sku_id' => AppHelper::generateHash() . '-' . $sku_id,
						'sku_title' => $sku_title,
						'sku_stock' => $sku_stock,
						'sku_price' => $sku_current_price_brl,
						'sku_image' => '', // Esta coluna é incluída abaixo
						'simplo7_sku' => '',
						'simplo7_sku_fornecedor' => '',
						'simplo7_quantidade' => '',
						'simplo7_valor_compra' => ''
					);

				}

				if ( !isset( $item_skus ) || count( $item_skus ) == 0 )
					$item_skus = '';

			endif;

		} catch ( Exception $e ) {

			return false;

		}


		if ( $should_download_images == true )
		{

			// 
			// Getting specific image from each variation
			// 

			if ( $total_skus > 0 ) :

				$page_content = file_get_contents( $product_url );
				$page_content = trim( preg_replace('/\s+/', ' ', $page_content) );

				$page_content_images = str_replace('<div data-role="msg-error" class="msg-error sku-msg-error">', 'end-variation-images', $page_content);

				preg_match("/(\<ul\ id\=\")(.+)(\"\ class\=\"sku\-attr\-list)(.*)(end\-variation\-images)/i", $page_content_images, $the_page_content_images);
				if ( isset($the_page_content_images[0]) && !empty($the_page_content_images[0]) ) {
					$the_page_content_images = $the_page_content_images[0];

					// 
					// For each variation, match an image in page's content with its variation ID
					// 

					for ( $i=0;$i<$total_skus;++$i ) :

						$the_new_id = explode(',',$item_skus[$i]['sku_id']);
						( isset($the_new_id[0]) && !empty($the_new_id[0]) ) ?
							$the_new_id = $the_new_id[0] :
							$the_new_id = $item_skus[$i]['sku_id'];

						// AppHelper::logThis('$the_new_id: '.$the_new_id);

						$full_image_url = Config::get('app.api_url').'products/';

						$item_skus[$i]['sku_image'] = $full_image_url.'default.jpg';

						preg_match("/(\<a\ data\-role\=\"sku\"\ data\-sku\-id\=\"" . $the_new_id . "\"\ id\=\".+\"\ title\=\".+\"\ href\=\"javascript\:\;\"\>)(.*)(\<\/li\>)/i", $the_page_content_images, $the_image);
						if ( isset($the_image[0]) && !empty($the_image[0]) ) {
							$the_image = $the_image[0];

							// AppHelper::logThis( '$the_image 1: '. $the_image);

							$the_image = explode('</li>', $the_image);
							if ( isset($the_image[0]) && !empty($the_image[0]) ) {
								$the_image = $the_image[0];

								$the_image = preg_replace('/<a data-role="sku" data-sku-id="'. $the_new_id .'" id="sku-1-'. $the_new_id .'" title=".+" href="javascript:;"><img src=".+" title=".+" bigpic="/', '', $the_image);
								// $the_image = preg_replace('/<a data-role="sku" data-sku-id="'.$item_skus[$i]['sku_id'].'" id="sku-1-'.$item_skus[$i]['sku_id'].'" title=".+" href="javascript:;"><img src=".+" title=".+" bigpic="/', '', $the_image);
								$the_image = preg_replace('/"\/><\/a>/', '', $the_image);

								// Download the Ali Express image, create an unique name and use its name as image name

								$variation_generated_name = AppHelper::downloadProductImage( $the_image );

								// 
								// Acho que aqui vai entrar o nome legível da variação que está no AliExpress
								// 

								$item_skus[$i]['sku_image'] = $full_image_url.$variation_generated_name;

							}

						}


					endfor;

				}


			endif;
			
			AppHelper::logThis( 'skus DEPOIS IMG: '. json_encode($item_skus) );

		}

		AppHelper::logThis( 'Rodou o cron_update_variations: ' . json_encode($item_skus) );

        return $item_skus;

		// end images for each variation


	}








	public function sync()
	{
		$token = Request::header('Custom');

		( Input::has('orders') ) ?
			$orders = Input::get('orders') :
			$orders = '';

		if ( !$token )
			return Response::json( array('status' => 'missing token') )->setCallback(Input::get('callback'));

		if ( empty($orders) )
			return Response::json( array('status' => 'missing orders to sync') )->setCallback(Input::get('callback'));

		$user = User::where('token','=',$token)->first();
		if ( !$user )
			return Response::json( array('status' => 'user not found') )->setCallback(Input::get('callback'));

		// 
		// Orders
		// [{"orderId":"88196882801527","orderStatus":"Aguardando Pagamento"}]
		// 

		$orders = json_decode($orders);
		$total_orders = count($orders);

		for ( $i=0;$i<$total_orders;++$i ) :

			$orderItem = OrderItem::where('ali_express_order_id', '=', $orders[$i]->orderId )
									->where('owner_id','=',$user->id)
									->first();
			if ( $orderItem ) :

				$orderItem->ali_express_order_shipping_status = $orders[$i]->orderStatus;
				$orderItem->save();

			endif;

		endfor;

		return Response::json( array('status' => 'success') )->setCallback(Input::get('callback'));

	}

	public function syncTrackingCodes()
	{

		( Input::has('token') ) ?
			$token = trim( Input::get('token') ) :
			$token = '';

		( Input::has('orderStatus') ) ?
			$orderStatus = trim( Input::get('orderStatus') ) :
			$orderStatus = '';

		( Input::has('orderId') ) ?
			$orderId = trim( Input::get('orderId') ) :
			$orderId = '';

		( Input::has('trackingCode') ) ?
			$trackingCode = trim( Input::get('trackingCode') ) :
			$trackingCode = '';

		if ( !$token )
			return Response::json( array('status' => 'missing token') )->setCallback(Input::get('callback'));

		if ( empty($orderId) )
			return Response::json( array('status' => 'missing order id to sync') )->setCallback(Input::get('callback'));

		$user = User::where('token','=',$token)->first();
		if ( !$user )
			return Response::json( array('status' => 'user not found') )->setCallback(Input::get('callback'));

		$order = OrderItem::where('owner_id','=',$user->id)
							->where('ali_express_order_id','=',$orderId)
							->update(
								array(
									'ali_express_order_shipping_status' => $orderStatus,
									'ali_express_order_tracking_code' => $trackingCode
								)
							);

		// Log::info('OrderId: '.$orderId.' shipping status: '.$orderStatus.' | | trackingCode: '.$trackingCode);

		return Response::json( array('status' => 'success') )->setCallback(Input::get('callback'));

	}

}
