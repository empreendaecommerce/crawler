<?php

class ProductController extends \BaseController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$token = Request::header('Custom');
		$user = UserController::getUserByToken( $token );
		$products = Product::where('owner_id','=',$user->id)
							->orderBy('id','DESC')
							->get();
		foreach ( $products as $item ) :
			$images = ProductImage::where('product_id','=',$item->id)
									->orderBy('id','ASC')
									->take(6)
									// ->remember(60)
									->get();
			( $images ) ?
				$item->images = $images :
				$item->images = '[]';
		endforeach;

		foreach ( $products as $item ) :
			$variations = ProductVariation::where('product_id','=',$item->id)
										->orderBy('id','ASC')
										// ->remember(60)
										->get();
			( $variations ) ?
				$item->variations = $variations :
				$item->variations = '[]';
		endforeach;

		return Response::json( $products )->setCallback(Input::get('callback'));
	}


	public function import()
	{
		$token = Request::header('Custom');

		( Input::has('product_url') ) ?
			$product_url = Input::get('product_url') :
			$product_url = '';

		$user = UserController::getUserByToken( $token );

		// if ( empty($product_url) || !AppHelper::is_url( $product_url ) )
		if ( empty($product_url) )
			return Response::json( array('status' => 'missing or wrong product url') )->setCallback(Input::get('callback'));

		$total_imported_products = Product::where('owner_id','=',$user->id)
										->count();

		if ( ( $total_imported_products >= Config::get('app.free_imports') ) && $user->payment_status != 'active' )
			return Response::json( array('status' => 'subscribe to paid version') )->setCallback(Input::get('callback'));

		// Check if product already has being imported by other user to use its infos
		$has_product = Product::where('owner_id','=',$user->id)
								->where('product_url','=',$product_url)
								->first();

		// 
		// If is localhost, bypass the "already exists" product condition
		// 
		if ( app()->env != 'local' ) {
			if ( $has_product )
				return Response::json( array('status' => 'product already imported') )->setCallback(Input::get('callback'));
		}


		// It's a new product
		// Fetch data from Ali Express and save it all
		$product_infos['title'] = 'Não foi possível obter o nome deste produto';

		// @PENTAGRAMA: MODIFICADO A CHAMADA
		/*
		 * AQUI PODE TER UMA VALIDAÇÃO POIS RETORNAMOS O VALOR "Titulo não encontrado"
		 * QUANDO O PRODUTO NÃO EXISTE
		 */
		$crawler = new AliExpressCrawler($product_url);
		$product_infos = $crawler->getBasicInfo();
		$create_this_product = array(
			'owner_id' => $user->id,
			'state' => 'imported',
			'product_url' => $product_url,
			'name' => ( empty($product_infos['title']) ? 'Sem nome' : $product_infos['title'] ),
			'description' => ''
		);

		// AppHelper::logThis( $create_this_product );

		$created = new Product();
		$created->__construct( $create_this_product );
		$created->save();

		$new_product_id = $created->id;

		// REMOVER ISSO
		// Product::find($new_product_id)->delete();

		// 
		// Images
		// 

		// @PENTAGRAMA: MODIFICADO A CHAMADA
		$product_images = $crawler->getImages();
		
		// @PENTAGRAMA: ADICIONADO O DOWNLOAD DA IMAGEM POR AQUI
		if ( !empty($product_images) ) {
			$images_array = [];
			foreach($product_images as $img) {
				$thumbnail_image = $img['thumbnail'];
				$thumbnail_generated_name = AppHelper::downloadProductImage( $thumbnail_image );

				// tem a imagem original usando $img['image'].

				$full_image_url = Config::get('app.api_url').'products/';
				$images_array[] = array(
					'product_id' => $product_id,
					'image_url' => $full_image_url.$thumbnail_generated_name
				);

			}
			ProductImage::insert( $product_images );
		}

		// AppHelper::logThis( $product_images );

		// 
		// Variations
		// 

		$product_variations = $crawler->getProductSKUs($new_product_id);
		if ( empty($product_variations) ) {

			$full_image_url = Config::get('app.api_url').'products/';
			AppHelper::logThis( json_encode($product_images) );
			$the_image = $full_image_url . 'default.jpg';

			if ( !empty($product_images) && count($product_images) > 0 ) {
				if ( isset($product_images[0]) && !empty($product_images[0]) )
					// @PENTAGRAMA: O ATRIBUTO FOI RENOMEADO PARA image
					$the_image = $product_images[0]['image'];
			}

			$the_hash = AppHelper::generateHash();

			$product_variations = array(
				'product_id' => $new_product_id,
				'sku_id' => $the_hash . '-' . $new_product_id,
				'sku_title' => sha1( $the_hash . $new_product_id ),
				'sku_stock' => 1,
				'sku_price' => 0,
				'sku_combinacao_id' => '', //
				'sku_name' => '',
				'sku_image' => $the_image,
				'simplo7_sku' => '',
				'simplo7_sku_fornecedor' => '',
				'simplo7_quantidade' => '',
				'simplo7_valor_compra' => ''
			);

		}
		ProductVariation::insert( $product_variations );

		// AppHelper::logThis( $product_variations );

		// Getting the new product

		$the_product = Product::where('owner_id','=',$user->id)
							->where('id','=',$new_product_id)
							// ->remember(60)
							->first();

		// New product images

		$images = ProductImage::where('product_id','=',$the_product->id)
								->orderBy('id','ASC')
								// ->remember(60)
								->take(6)
								->get();

		( $images ) ?
			$the_product->images = $images :
			$the_product->images = '[]';

		// New product variations
			
		$variations = ProductVariation::where('product_id','=',$the_product->id)
									->orderBy('id','ASC')
									// ->remember(60)
									->get();

		( $variations ) ?
			$the_product->variations = $variations :
			$the_product->variations = '[]';

		return Response::json( $the_product )->setCallback(Input::get('callback'));

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showProductImages( $product_id )
	{

		if ( !$product_id )
			return Response::json( array('status' => 'missing product id') )->setCallback(Input::get('callback'));

		$response = ProductImage::where('product_id','=',$product_id)
								->orderBy('id','ASC')
								->get();

		return Response::json( $response )->setCallback(Input::get('callback'));

	}

	public function showProductVariations( $product_id )
	{

		if ( !$product_id )
			return Response::json( array('status' => 'missing product id') )->setCallback(Input::get('callback'));

		$response = ProductVariation::where('product_id','=',$product_id)
									->orderBy('id','ASC')
									->get();

		return Response::json( $response )->setCallback(Input::get('callback'));

	}

	public function remove()
	{
		$token = Request::header('Custom');
		
		( Input::has('product_id') ) ?
			$product_id = trim( Input::get('product_id') ) :
			$product_id = '';
		
		$user = UserController::getUserByToken( $token );

		if ( empty($product_id) )
			return Response::json( array('status' => 'missing product id') )->setCallback(Input::get('callback'));

		$product = Product::where('id','=',$product_id)
							->where('owner_id','=', $user->id)
							->first();

		if ( !$product )
			return Response::json( array('status' => 'product not found or not allowed') )->setCallback(Input::get('callback'));

		if ( $product->state != 'imported' )
			return Response::json( array('status' => 'product state not allowed') )->setCallback(Input::get('callback'));

		$product->forceDelete();

		// 
		// Getting images to unlink()
		// 

		$images = ProductImage::where('product_id','=',$product_id)->get();

		if ( !empty($images) ) {

			foreach ( $images as $item ) :

				$image = str_replace( url('products') . '/', '', $item->image_url );
				$full_path = public_path('products') . '/' . $image;
				if ( $image != 'default.jpg' && file_exists( $full_path ) )
					unlink( $full_path );

			endforeach;

		}

		// 
		// Getting imagens of variations to unlink()
		// 

		$variations = ProductVariation::where('product_id','=',$product_id)->get();

		if ( !empty($variations) ) {

			foreach ( $variations as $item ) :

				$image = str_replace( url('products') . '/', '', $item->sku_image );
				$full_path = public_path('products') . '/' . $image;
				if ( $image != 'default.jpg' && file_exists( $full_path ) )
					unlink( $full_path );

			endforeach;

		}

		// Delete rows images
		ProductImage::where('product_id','=',$product_id)->forceDelete();

		// Delete rows variations
		ProductVariation::where('product_id','=',$product_id)->forceDelete();

		return Response::json( array('status' => 'success') )->setCallback(Input::get('callback'));
	}


	public function updateState()
	{
		$token = Request::header('Custom');
		
		( Input::has('product_id') ) ?
			$product_id = trim( Input::get('product_id') ) :
			$product_id = '';

		( Input::has('store_category') ) ?
			$store_category = trim( Input::get('store_category') ) :
			$store_category = '';
		
		$user = UserController::getUserByToken( $token );

		if ( empty($product_id) )
			return Response::json( array('status' => 'missing product id') )->setCallback(Input::get('callback'));

		if ( empty($store_category) )
			return Response::json( array('status' => 'missing category id') )->setCallback(Input::get('callback'));

		$product = Product::where('id','=',$product_id)
							->where('owner_id','=', $user->id)
							->first();

		$setting = Setting::where('user_id','=',$user->id)->first();
		if ( !isset($setting) )
			return Response::json( array('status' => 'setting not found') )->setCallback(Input::get('callback'));

		if (
			!isset($setting->id_loja) || empty($setting->id_loja) || 
			!isset($setting->simplo7_appkey) || empty($setting->simplo7_appkey)
		)
			return Response::json( array('status' => 'missing settings fields') )->setCallback(Input::get('callback'));

		if ( !$product )
			return Response::json( array('status' => 'product not found or not allowed') )->setCallback(Input::get('callback'));

		$current_product_state = $product->state;

		if ( $current_product_state != 'imported' )
			return Response::json( array('status' => 'unknown product state') )->setCallback(Input::get('callback'));

		// Send to store
		// Change state to stock
		$product->state = 'stock';
		$product->category_id = $store_category;
		$product->save();

		Simplo7Controller::create( $token, $product_id );

		return Response::json( $product )->setCallback(Input::get('callback'));
	}

	public function updateSupplier()
	{

		$token = Request::header('Custom');
		
		( Input::has('product_id') ) ?
			$product_id = trim( Input::get('product_id') ) :
			$product_id = '';

		( Input::has('product_url') ) ?
			$product_url = trim( Input::get('product_url') ) :
			$product_url = '';
		
		$user = UserController::getUserByToken( $token );

		if ( empty( $product_id ) )
			return Response::json( array('status' => 'missing product id') )->setCallback(Input::get('callback'));

		if ( empty( $product_url ) )
			return Response::json( array('status' => 'missing product url') )->setCallback(Input::get('callback'));

		$product = Product::where('id','=',$product_id)->update( array('product_url' => $product_url) );

		return Response::json( array('status' => 'success') )->setCallback(Input::get('callback'));
	}




	public function updateVariation()
	{

		$token = Request::header('Custom');
		
		( Input::has('variation') ) ?
			$variation = trim( Input::get('variation') ) :
			$variation = '';

		( Input::has('combinacao_id') ) ?
			$combinacao_id = trim( Input::get('combinacao_id') ) :
			$combinacao_id = '';

		( Input::has('sku_name') ) ?
			$sku_name = trim( Input::get('sku_name') ) :
			$sku_name = '';

		$user = UserController::getUserByToken( $token );

		if ( empty( $variation ) )
			return Response::json( array('status' => 'missing variation id') )->setCallback(Input::get('callback'));

		if ( empty( $sku_name ) )
			return Response::json( array('status' => 'missing sku name') )->setCallback(Input::get('callback'));

		ProductVariation::where('id','=',$variation)->update( array('sku_combinacao_id' => $combinacao_id, 'sku_name' => $sku_name) );

		Cache::flush();

		return Response::json( array('status' => 'success') )->setCallback(Input::get('callback'));
	}




	public function getKPIs()
	{
		$token = Request::header('Custom');

		$user = UserController::getUserByToken( $token );

		$orders = Order::where('owner_id','=', $user->id)
							->get();

		$total_orders_has_pending = 0;

		if ( !empty($orders) ) :
			foreach ( $orders as $order ) :
				$itens = OrderItem::where('order_id','=',$order->id)
									->where('ali_express_order_id','=','')
									->first();

				if ( !empty($itens) ) :

					++$total_orders_has_pending;

				endif;
			endforeach;
		endif;

		$total_sales_today = Order::where('owner_id','=', $user->id)
						->whereRaw("simplo7_data_pedido >= CONCAT(DATE(NOW()),' 00:00:00')")
						->select(
							array(
								DB::raw("SUM(orders.simplo7_total_pedido) AS total_sales_today")
							)
						)
						->pluck('total_sales_today');

		$response = array(
			'total_pending' => $total_orders_has_pending,
			'total_sales_today' => $total_sales_today
		);

		return Response::json( $response )->setCallback(Input::get('callback'));

	}

}
