<?php

namespace Truo\Crawler;

use \Truo\Crawler\CrawlerAbstract;

/**
 * Classe que representa o Crawler para o AliExpress
 * 
 * @author Pentagrama
 * @package Crawler
 * @subpackage AliExpress
 */
class Aliexpress extends CrawlerAbstract {
	protected $shippingUrl = 'https://freight.aliexpress.com/ajaxFreightCalculateService.htm';
	protected $_cached = [];

	/**
	 * Função que recebe as possíveis origens para o envio do produto
	 * e retorna um vetor com os detalhes de cada método de entrega 
	 * 
	 * @access public 
	 * @param Integer $objectId
	 * @param Array $origins
	 * @param Integer $quantity
	 * @return Array 
	 */ 
	public function getShippingInfo($objectId, $origins, $quantity)
	{
		$returnArr = array();
		//Para cada origem o sistema busca as informações de métodos de envio
		if(is_array($origins)){
			foreach($origins as $key => $value){
				$url = $this->shippingUrl.'?callback=jQuery18309619804169999404_1522086687022&f=d&productid='.$objectId.'&count='.$quantity.'&currencyCode=BRL&sendGoodsCountry='.$value.'&country=BR&province=&city=&abVersion=1&_='.time();

				$str = file_get_contents($url);
				preg_match('#\((.*?)\)#', $str, $match);
				$detailed = json_decode($match[1],true);
				$returnArr[$value] = $detailed;
			}
		}
		/*
		 * Se ele não voltar métodos de envio significa que o 
		 * envio não é realizado para o Brasil
		 */
		return $returnArr;
	}

	/**
	 * Função que obtém os dados básicos do produto como o título e o seu id 
	 * 
	 * @access public 
	 * @return Array 
	 */ 
	public function getBasicInfo()
	{
		/*
		 * Query para obter o título do produto, se não encontrado, 
		 * significa que o produto não existe mais, logo não tem disponibilidade
		 */
		$tableRows = $this->xpath->query('//h1[@class="product-name"]');

		if(!$tableRows->item(0))
			return 'not_found';

		$title = $tableRows[0]->textContent;
		if(!$title) {
			$title = 'Título não encontrado';
		}

		//Query para retornar o ID do produto
		$tableRows = $this->xpath->query('//input[@name="objectId"]/@value');
		$objectId = $tableRows[0]->nodeValue;
		$returnArr = array(
			'title' => $title, 
			'objectId' => $objectId
		);

		return $returnArr;
	}

	/**
	 * Função que busca as imagens cadastradas para um produto 
	 * 
	 * @access public 
	 * @return Array 
	 */ 
	public function getImages()
	{
		/*
		 * Query para retornar todos os spans que armazenam 
		 * as imagens dos thumbails do produto
		 */
		$tableRows = $this->xpath->query('//span[@class="img-thumb-item"]');
		$rows = $tableRows;
		$returnArr = array();
		$i = 0;
		foreach($rows as $row) {
			//Subquery que retorna o valor do atributo src da imagem dentro do span
			$img = $this->xpath->query('img/@src',$row);
			$thumbnail = $img[0]->nodeValue;
			$returnArr[$i]['thumbnail'] = $thumbnail;
			$returnArr[$i]['image'] = $this->_transformThumbToImage($thumbnail);
			$i++;
		}
		return $returnArr;
	}

	/**
	 * Função que retorna se o produto tem múltiplas origens de envio 
	 * 
	 * @access public 
	 * @return Array 
	 */ 
	public function getOrigins()
	{
		/*
		 * Query que retorna as tags a que possuem as múltiplas 
		 * origens de envio se existentes
		 */
		$tableRows = $this->xpath->query('//a[@data-role="sku" and @data-code]');
		$rows = $tableRows;
		$returnArr = array();
		foreach($rows as $row){
			//Subquery para pegar o atributo data-code do a que contem a origem
			$origin = $this->xpath->query('@data-code',$row);
			$returnArr[] = $origin[0]->textContent;
		}
		//Quando não possui opções de método de envio ele insere o CN (China) como padrão
		if(array_search('CN', $returnArr) === false){
			$returnArr[] = 'CN';
		}
		return $returnArr;
	}

	/** 
	 * Função que retorna os detalhes das variações do produto
	 * 
	 * @access public 
	 * @param Integer $product_id
	 * @return Array 
	 */ 
	public function getProductSKUs($product_id)
	{
		$pageContent = file_get_contents( $this->productUrl );
		$pageContent = trim( preg_replace('/\s+/', ' ', $pageContent) );

		preg_match("/(var\ skuProducts\=\[\{)(.*)(}\}\]\;)/i", $pageContent, $skus);
		if ( !isset($skus[2]) || empty($skus[2]) )
			return false;

		$skus = $skus[2];
		$skusJson = '[{'.$skus.'}}]';
		$skusJson = json_decode( $skusJson );
		$totalSkus = count( $skusJson );
		$itemSkus = array();
		$imgIndex = 0;
		for ( $is=0;$is<$totalSkus;++$is ) {

			$skuId = $skusJson[$is]->skuPropIds;
			$skuTitle = !empty($skusJson[$is]->skuAttr) ? $skusJson[$is]->skuAttr : null;
			// $skuTitle = $skusJson[$is]->skuAttr;
			$skuStock = $skusJson[$is]->skuVal->availQuantity;

			//Testa se o produto tem promoção ativa
			if($skusJson[$is]->skuVal->isActivity) {
				//Se possui, pega o valor atual da promoção
				$skuCurrentPriceBrl = $skusJson[$is]->skuVal->actSkuMultiCurrencyCalPrice;
			} else {
				//Senão pega o valor padrão do produto
				$skuCurrentPriceBrl = $skusJson[$is]->skuVal->skuMultiCurrencyCalPrice;
			}

			$varSku = explode(',', $skuId);
			$thumbnail = '';
			$image = '';
			if($imgIndex === 0){
				if(is_array($varSku)){
					foreach($varSku as $key => $value){
						//Query que retorna as imagens das variações caso existam
						$tableRows = $this->xpath->query('//a[@data-sku-id="'.$value.'"]//img/@src');
						if($tableRows && is_object($tableRows[0]) && $tableRows[0]->nodeValue){
							$thumbnail = $tableRows[0]->nodeValue;
							$image = $this->_transformThumbToImage($thumbnail);
							break;
						} else {
							$imgIndex++;
						}
					}
				}
			}else{
				//Query que retorna as imagens das variações caso existam
				if (isset($varSku[$imgIndex])) {
					$tableRows = $this->xpath->query('//a[@data-sku-id="'.$varSku[$imgIndex].'"]//img/@src');
					if($tableRows && $tableRows[0]->nodeValue){
						$thumbnail = $tableRows[0]->nodeValue;
						$image = $this->_transformThumbToImage($thumbnail);
					}
				} else {
					if(is_array($varSku)){
						foreach($varSku as $key => $value){
							//Query que retorna as imagens das variações caso existam
							$tableRows = $this->xpath->query('//a[@data-sku-id="'.$value.'"]//img/@src');
							if($tableRows && is_object($tableRows[0]) && $tableRows[0]->nodeValue){
								$thumbnail = $tableRows[0]->nodeValue;
								$image = $this->_transformThumbToImage($thumbnail);
								break;
							} else {
								$imgIndex = 0;
							}
						}
					}
				}
			}

			//Cria detalhse das variações, informando quais são seus atributos
			$skuOptionsValue = array();
			$skuOptions = explode(';', $skuTitle);
			if(is_array($skuOptions)){
				foreach($skuOptions as $key => $value){
					$skuOptionsDetailed = explode('#', $value);
					$skuOptionsDetailedInside = explode(':', $skuOptionsDetailed[0]);
					if (count($skuOptionsDetailedInside) > 1) {
						$skuOptionsValue[$skuOptionsDetailedInside[0]] = $skuOptionsDetailedInside[1];
					}
					// $skuOptionsValue[$skuOptionsDetailedInside[0]] = $skuOptionsDetailedInside[1];
				}
			}

			//Cria detalhse das variações, informando quais são seus atributos
			$skuOptionsValue = array();
			$skuOptions = explode(';', $skuTitle);
			if(is_array($skuOptions)){
				foreach($skuOptions as $key => $value){
					$skuOptionsDetailed = explode('#', $value);
					$skuOptionsDetailedInside = explode(':', $skuOptionsDetailed[0]);
					if (count($skuOptionsDetailedInside) > 1) {
						$skuOptionsValue[$skuOptionsDetailedInside[0]] = $skuOptionsDetailedInside[1];
					}
					// $skuOptionsValue[$skuOptionsDetailedInside[0]] = $skuOptionsDetailedInside[1];
				}
			}

			//Vetor com os detalhes das variações do produto
			$itemSkus[] = array(
				'product_id' => $product_id,
				'sku_id' => $skuId,
				'sku_title' => $skuTitle,
				'sku_stock' => $skuStock,
				'sku_price' => $skuCurrentPriceBrl,
				'sku_image' => $image,
				'sku_thumbnail' => $thumbnail,
				'sku_options' => $skuOptionsValue,
				'simplo7_sku' => '',
				'simplo7_sku_fornecedor' => '',
				'simplo7_quantidade' => '',
				'simplo7_valor_compra' => ''
			);

		}

		return $itemSkus;
	}

	/**
	 * Retorna os atributos separados por titulo e opções
	 * 
	 * @return array
	 */
	public function getAttributes()
	{
		$tableRows = $this->xpath->query('//dl[@class="p-property-item"]');
		$rows = $tableRows;
		$returnArr = array();
		foreach($rows as $row){
				//Subquery para pegar o atributo data-code do a que contem a origem
			$title = $this->xpath->query('dt/text()',$row);
			$title = trim($title[0]->textContent, ':');

			$attributesUl = $this->xpath->query('dd//ul//@data-sku-prop-id',$row);
			$attributes = $this->xpath->query('dd//ul//li//a',$row);

			foreach($attributes as $attribute){
				$node = $this->xpath->query('span//text()',$attribute);
				if(!$node[0] || !$node[0]->textContent){
					$node = $this->xpath->query('@title',$attribute);
				}
				// $returnArr[$title][] = $node[0]->textContent;
				$returnArr[$title][] = $node->item(0)->textContent;
				$dataSkuId = $this->xpath->query('@data-sku-id',$attribute);
				$returnArr[$attributesUl[0]->textContent]['name'] =  $title;
				$returnArr[$attributesUl[0]->textContent]['values'][$dataSkuId[0]->textContent] = $node[0]->textContent;

			}
		}
		return $returnArr;
	}

	/**
	 * Retira o final do nome da imagem, que transforma
	 * em thumbnail, ex.: imagem.jpg_50x50.jpg -> imagem.jpg
	 *
	 * @param string $thumbnail
	 * @return string
	 */
	protected function _transformThumbToImage($thumbnail) {
		$image = explode('_', $thumbnail, '-1');
		$image = implode('_', $image);

		return $image;
	}
}
