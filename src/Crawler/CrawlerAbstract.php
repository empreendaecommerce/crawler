<?php

namespace Truo\Crawler;

use DOMDocument;
use DOMXPath;

/**
 * Classe abstrata para definição básica do crawler
 * @author Pentagrama
 * @package Crawler
 */
abstract class CrawlerAbstract {

	/**
	 * URL processada do produto
	 * 
	 * @var string
	 */
	protected $productUrl;

	/**
	 * Instancia do XPath para ser utilizada
	 * 
	 * @var DOMXPath
	 */
	protected $xpath;
	
	/** 
	 * Construtor que inicia o crawler a partir da URL do produto 
	 * 
	 * @access public 
	 * @param String $url
	 * @return void 
	 */ 
	public function __construct($url){
		$this->productUrl = $url;

		$curl = curl_init($this->productUrl); curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10');
		$html = curl_exec($curl); 
		curl_close($curl);

		$dom = new DOMDocument();
		@$dom->loadHTML($html);
		$xpath = new DOMXPath($dom);

		$this->xpath = $xpath;
	}

	/**
	 * Deve retornar informações básicas do produtO
	 * como titulo (title) e identificado unico do
	 * produto (objectId)
	 * 
	 * @abstract
	 * @return array
	 */
	abstract protected function getBasicInfo();

	/**
	 * Deve retornar as imagens do produto com 
	 * thumbnail e imagem original (são imagens de destaque)
	 * 
	 * @abstract
	 * @return array
	 */
	abstract protected function getImages();

	/**
	 * Deve retornar as variações do produto contendo
	 * ID da variação, ID do produto, titulo, estoque,
	 * preço e imagens
	 * 
	 * @abstract
	 * @return array
	 */
	abstract protected function getProductSKUs($product_id);

}
