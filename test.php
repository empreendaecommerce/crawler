<form method="POST">
	<label>Digite a URL do produto</label>
	<input name="url" size=120 value="<?php echo $_POST['url'] ?>" />
</form>

<?php
require 'vendor/autoload.php';

 ini_set('display_errors', 1);
 ini_set('display_startup_errors', 1);
 error_reporting(E_ALL);

/*ini_set("xdebug.var_display_max_children", -1);
ini_set("xdebug.var_display_max_data", -1); 
ini_set("xdebug.var_display_max_depth", -1);*/

if($_POST['url']){

	$aeCrawler = new Truo\Crawler\Aliexpress($_POST['url']);

	$basicInfoArr = $aeCrawler->getBasicInfo();
	$images = $aeCrawler->getImages();
	$skus = $aeCrawler->getProductSKUs($basicInfoArr['objectId']);
	$origins = $aeCrawler->getOrigins();
	$shippingInfo = $aeCrawler->getShippingInfo($basicInfoArr['objectId'], $origins, 1);
	$attributes = $aeCrawler->getAttributes();

	echo '<h2>Informações Básicas do Produto</h2>';
	var_dump($basicInfoArr);
	echo '<h2>Imagens</h2>';
	var_dump($images);
	echo '<h2>Variações do Produto</h2>';
	var_dump($attributes);
	echo '<h2>Detalhes das Variações</h2>';
	var_dump($skus);
	echo '<h2>Origens de Envio</h2>';
	var_dump($origins);
	echo '<h2>Informações sobre Entrega</h2>';
	var_dump($shippingInfo);
}
